#include <vector>
#include <tuple>
#include <functional>
#include<complex>


#include "../cquad.hpp"
#include "catch.hpp"


using namespace std;
using namespace std::complex_literals;

using cx_double = complex<double>;

TEST_CASE( "cquad core", "[core]" )
{

    SECTION( "test integration battery" ){

        auto f1 = [](double x){return exp(x);};
        auto f3 = [](double x){return sqrt(x);};
        auto f4 = [](double x){return 23/ (double) 25 * cosh(x) - cos(x);};
        auto f5 = [](double x){return 1. / (double) ( x * x * (x * x + 1) + 0.9);};
        auto f6 = [](double x){return x * sqrt(x);};


        vector<tuple<function<double(double)>, double, double, double>> battery;
        battery.push_back(make_tuple(f1, 0, 1, 1.7182818284590452354));
        battery.push_back(make_tuple(f3, 0, 1, 2./3.));
        battery.push_back(make_tuple(f4, -1, 1, 0.4794282266888016674));
        battery.push_back(make_tuple(f5, -1, 1, 1.5822329637296729331));
        battery.push_back(make_tuple(f6, 0, 1, 0.4));

        vector<double> rtols{1e-3, 1e-6, 1e-9, 1e-12};
        double atol = 0;

        srand(1);  // set random seed

        for (auto const& [f, a, b, exact] : battery){
            for (auto const& rtol : rtols){

                auto it = Cquad<double>(f, a, b);
                auto [igral, err] = it.improve_until(rtol, atol);

                //cout << "igral= " << igral << " exact= " << exact << " error= " << err << " true error= " << std::abs(igral - exact) << "\n";

                // test the total integral
                REQUIRE( std::abs(igral - exact) <= rtol );

                // test the interpolant
                for (int i = 0; i < 10; i++){  // evaluate the interpolant at random points in interval [a, b]
                    auto xs = a + (b - a) * (double) rand() / RAND_MAX;
                    auto fx = it(xs);
                    auto fx_ref = f(xs);
                    REQUIRE( std::abs(fx - fx_ref) <= rtol );
                }
                // REQUIRE( err <= rtol ); // does not work for f = f5 and rtols = 1e-9
            };
        }
    }

    SECTION( "test complex function" ){

        auto tol = 1E-5;

        auto f1 = [](double x) -> cx_double {return cos(x) + 1j * sin(x);};

        auto it = Cquad<cx_double>(f1, 0, 1);
        it.improve_until(tol, tol);

        srand(1);  // set random seed
        for (int i = 0; i < 10; i++){
            auto xs = (double) rand() / RAND_MAX;
            auto fx = it(xs);
            auto fx_ref = f1(xs);
            REQUIRE( std::abs(fx - fx_ref) <= tol );
        }
    }

}

