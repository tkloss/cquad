#define CATCH_CONFIG_MAIN
#include <vector>
#include <complex>
#include <tuple>

#include "../cquad.hpp"
#include "catch.hpp"


using namespace std;

double abs_error = 1.0E-15;


TEST_CASE( "cquad integration", "[cquad]" )
{

    SECTION( "test legendre" ){

        auto legs = legendre(11);
        vector<tuple<vector<frac_t>, vector<int>, int>> comparison;
        comparison.push_back(make_tuple(legs[0], vector<int>{1}, 1));
        comparison.push_back(make_tuple(legs[1], vector<int>{0, 1}, 1));
        comparison.push_back(make_tuple(legs[10], vector<int>{-63, 0, 3465, 0, -30030, 0, 90090, 0, -109395, 0, 46189}, 256));
        for (auto const& [a, b, div] : comparison){
            for (int i = 0; i < a.size(); i++) {
                auto c = a[i];
                auto d = b[i];
                assert (c * div == d);
            }
        }
    }

    SECTION( "test scalar_product" ){

        int n = 33;
        auto legs = legendre(n);

        auto selection = vector<int>{0, 5, 7, n-1};
  
        for (auto const& i : selection)
            for (auto const& j : selection){
                auto result = scalar_product(legs[i], legs[j]);
                if (i == j){
                    auto result_ref = frac_t(2, 2*i + 1 );
                    assert (result == result_ref);
                } else {
                    assert (result == 0);
                }
            }

    }


    SECTION( "test_ ewton" ){

        auto nwton = newton(9);
        auto nwton_ref = vector<double>{0, 0.0625, 0, -0.6875, 0. ,  2.125,  0 , -2.5,  0,  1};
        
        for (int i = 0; i < nwton.size(); i++){
            REQUIRE( abs(boost::rational_cast<double>(nwton[i]) - nwton_ref[i]) <= abs_error );
        }
    }



    SECTION( "test newton_legendre" ){

        int level = 1;
        auto params = ParamsCquad();

        auto legs = legendre(params.sizes[level] + 1);
        auto newton_coeffs = newton_legendre(params.sizes)[level];
        vector<double> result(legs.back().size(), 0);
        for (int i = 0; i < legs.size(); i++){
            auto leg = legs[i];
            auto factor = std::sqrt((2.0 * leg.size() - 1) / (double) 2) * newton_coeffs[i];
            for (int j = 0; j < leg.size(); j++)
                result[j] += factor * boost::rational_cast<double>(leg[j]);
        }

        auto nwtn = newton(params.sizes[level]);
        for (int i = 0; i < result.size(); i++){
            auto res = result[i];
            auto res_ref = boost::rational_cast<double>(nwtn[i]);
            REQUIRE( abs(res - res_ref) <= abs_error );  // TODO: accuracy!
        }
    }

    SECTION( "test vandermonde" ){

        auto nodes = ParamsCquad().nodes;
        
        auto mat = vandermonde(nodes[0]);
        arma::mat mat_ref(3, 3);
        mat_ref.row(0) = arma::rowvec{ 0.7071067811865476, -1.224744871391589 ,  1.5811388300841898};
        mat_ref.row(1) = arma::rowvec{ 0.7071067811865476,  0.                , -0.7905694150420949};
        mat_ref.row(2) = arma::rowvec{ 0.7071067811865476,  1.224744871391589 ,  1.5811388300841898};
        REQUIRE(arma::max(arma::max(arma::abs(mat - mat_ref))) <= abs_error );

    }


    SECTION( "test calc_coeffs" ){

        auto f=[](const vector<double>& x){
            vector<double> res;
            for (auto const& xx : x)
                res.push_back(cos(xx));
            return res;
        };

        int level = 3;
        auto params = ParamsCquad();
        
        auto x = params.nodes[level];
        auto fx = f(x);
        
        auto coeffs = calc_coeffs(fx, params.nodes[level], params.inv_Vs[level], params.newton_coeffs[level], params.alpha, params.gamma);
        auto coeffs_ref = vector<double>{1.1900196790587718e+00,  7.7195194680967916e-17,
                                        -1.9617205912295108e-01,  5.3776427755281020e-17,
                                         4.2893768040969732e-03, -1.5612511283791264e-17,
                                        -3.6493357902626228e-05,  1.1275702593849246e-17,
                                         1.6481178545958014e-07,  1.0408340855860843e-17,
                                        -4.6120531005389154e-10,  6.9388939039072284e-18,
                                         8.7800947068394919e-13,  0.0000000000000000e+00,
                                        -1.1865508575681361e-15,  2.7755575615628914e-17,
                                        -1.2143064331837650e-17};

        for (int i = 0; i < coeffs.size(); i++){
            REQUIRE( abs(coeffs[i] - coeffs_ref[i]) <= abs_error );
        }
    }


    SECTION( "test interval" ){

        auto f=[](vector<double> x){
            vector<double> res;
            for (auto const& xx : x)
                res.push_back(cos(xx));
            return res;
        };

        double a = -1;
        double b = 1;
        int level = 3;
        int depth = 0;

        auto params = ParamsCquad();
        auto ival = Interval<double>(f, a, b, level, depth, params);

        REQUIRE( ival.a == a );
        REQUIRE( ival.b == b );
        REQUIRE( ival.level == level );
        REQUIRE( ival.depth == 0 );

        ival.interpolate();

        REQUIRE( std::abs(ival.igral - sin(b) + sin(a)) <= abs_error );

        auto refine = ival.refine();

        REQUIRE( std::abs(ival.igral - sin(b) + sin(a)) <= abs_error );
        
        REQUIRE( ival.err <= abs_error );
        REQUIRE( ival.level == level + 1);
        REQUIRE( refine == false );

        auto [i0, i1] = ival.split();
 
        REQUIRE( std::abs(i0.igral - sin(b)) <= 1.0E-6 );
        REQUIRE( std::abs(i1.igral + sin(a)) <= 1.0E-6 );
        REQUIRE( i0.a == a );
        REQUIRE( i0.b == 0 );
        REQUIRE( i1.a == 0 );
        REQUIRE( i1.b == b );
        REQUIRE( i0.depth == 1 );
        REQUIRE( i1.depth == 1 );
        REQUIRE( i0.level == params.min_level );
        REQUIRE( i1.level == params.min_level );

    }


    SECTION( "test cquad" ){

        auto f=[](vector<double> x){
            vector<double> res;
            for (auto const& xx : x)
                res.push_back(cos(xx));
            return res;
        };

        double a = -1, b = 1;
        auto it = Cquad<double>(f, a, b);
        auto [igral, err] = it.improve_until(abs_error, abs_error);

        REQUIRE( std::abs(igral - sin(b) + sin(a)) <= abs_error );
        REQUIRE( err <= abs_error );

    }


}

