#ifndef CQUAD_H
#define CQUAD_H

#include <vector>
#include <map>
#include <list>
#include <math.h>
#include <algorithm>
#include <functional>
#include <iostream>
#include <cassert>

#include <armadillo>
#include <boost/rational.hpp>
#include <boost/multiprecision/cpp_int.hpp>


using std::vector;
using std::pair;
using std::tuple;
using std::function;
using std::cout;

const double SQRT_ONE_HALF = sqrt(0.5);
const double SQRT_THREE_HALF = sqrt(1.5);
const int MAX_LEVEL = 4;

// long long int (64 bit) is not long enough for 33 nodes, floating point is not precise enough
using int_t = boost::multiprecision::cpp_int;
using frac_t = boost::rational<int_t>;

//-------------------------------------------------------- helper routines

inline vector<vector<frac_t>> legendre(int n) {
    // Return the first n Legendre polynomials.

    if (n < 1) throw std::invalid_argument("legendre: n < 1");

    vector<vector<frac_t>> result;

    result.push_back({frac_t(1)});
    if (n == 1) return result;

    result.push_back({frac_t(0), frac_t(1)});
    if (n == 2) return result;

    for (int i = 2; i < n; i++) {
        vector<frac_t> nextp(i + 1, frac_t(0));
        auto rb = result[i - 1];
        auto rb2 = result[i - 2];
        for (int j = 1; j < i + 1; j++)
            nextp[j] = rb[j-1] * (2 * i - 1);
        for (int j = 0; j < i - 1; j++)
            nextp[j] -= rb2[j] * (i - 1);
        for (int j = 0; j < i + 1; j++)
            nextp[j] /= i;
        result.push_back(nextp);
    }
    return result;
}


inline vector<frac_t> newton(int n) {
    // Compute the monomial coefficients of the Newton polynomial over the
    // nodes of the n-point Clenshaw-Curtis quadrature rule.

    if (n < 1) throw std::invalid_argument("newton: n < 1");

    int mod = 2 * (n-1);
    std::map<std::pair<int, int>, int> terms;
    terms[{0, 0}] = 1;

    vector<int> c(n + 1, 0);

    for (int i = 0; i < n; i++) {
        vector<tuple<int, int, int>> newterms;
        for (auto const& [key, m] : terms){
            auto[d, a] = key;
            for (auto const& b : {i, -i}){
                // in python: arg = (a + b) % mod, as % is "true" modulo operation, while % in C is remainder operation
                auto arg = (((a + b) % mod) + mod) % mod;
                if (arg > n-1)
                    arg = mod - arg;
                if (arg >= n / 2){
                    if (n % 2 && arg == n / 2)
                        continue;  // Zero term: ignore
                    newterms.push_back(std::make_tuple(d + 1, n - 1 - arg, -m));
                } else {
                    newterms.push_back(std::make_tuple(d + 1, arg, m));
                }
            }
        }
        for (auto const& [d, s, m] : newterms){
            auto key = std::make_pair(d, s);
            if (terms.contains(key)){
                terms[key] += m;
            } else {
                terms[key] = m;
            }
        }    
    }
    
    for (auto const& [key, m] : terms){
        auto[d, a] = key;
        if (m != 0 && a != 0) throw std::runtime_error("newton: Newton polynomial cannot be represented exactly.");
        c[n - d] += m;
    }

    vector<frac_t> cf(n + 1);
    for (int i = n; i >= 0; i--)
        cf[n - i] = frac_t(c[n - i], (int_t) pow(2, i));
    return cf;
}


inline frac_t scalar_product(const vector<frac_t>& a, const vector<frac_t>& b) {
    // Compute the polynomial scalar product int_-1^1 dx a(x) b(x).
    int la = a.size();
    int lb = b.size();
    auto lc = lb + la + 1;

    vector<frac_t> c(lc);
    for (int i = 0; i < lb; i++) {
        auto bi = b[i];
        if (bi == 0) continue;
        for (int j = i % 2; j < la; j += 2)
            c[i + j] += a[j] * bi;
    }
    frac_t sum(0);
    for (int i = 0; i < lc; i += 2)
        sum += c[i] * frac_t(1, i + 1);
    return 2 * sum;
}


inline vector<vector<double>> newton_legendre(const vector<int>& sizes) {
    // Calculate the decompositions of Newton polynomials (over the nodes
    // of the n-point Clenshaw-Curtis quadrature rule) in terms of
    // Legandre polynomials.

    auto legs = legendre(*std::max_element(sizes.begin(), sizes.end()) + 1);
    vector<vector<double>> result;
    for (const auto& n : sizes){
        vector<double> poly;
        auto a = newton(n);
        for (int i = 0; i < n + 1; i++){
            auto b = legs[i];
            auto igral = boost::rational_cast<double>(scalar_product(a, b));
            poly.push_back(std::sqrt((b.size() - 0.5)) * igral );
        }
        result.push_back(poly);
    }
    return result;
}


inline vector<double> chebyshev_nodes(int n){
    vector<double> x(n);
    for (int i = 0; i < n; i++){
        if (i == n / 2 && n % 2 != 0){
            x[i] = 0;
        } else {
            x[i] = - cos(M_PI * i / (n - 1));
        }
    }
    return x;
}


inline arma::mat vandermonde(const vector<double>& nodes){
    int n = nodes.size();
    arma::mat V(n, n);
    V.row(0) = arma::rowvec(n, arma::fill::ones);
    V.row(1) = arma::rowvec{nodes};
    for (int i = 2; i < n; i++){
        arma::rowvec row(n);
        for (int j = 0; j < n; j++)
            row(j) = (2*i-1) / (double) i * nodes[j] * V(i-1,j) - (i-1) / (double) i * V(i-2,j);
        V.row(i) = row;
    }
    for (int i = 0; i < n; i++)
        V.row(i) *= sqrt(i + 0.5);
    return V.t();
}


template< typename T >
inline T eval_legendre(const vector<T>& c, double x) {
    // Evaluate _orthonormal_ Legendre polynomial.
    // This uses the three-term recurrence relation from page 63 of Perdo
    // Gonnet's thesis.

    T c0, c1;
    int n = c.size();
    assert(n != 0);
    if (n == 1){
        c0 = c[0];
        c1 = 0;
    } else {
        c0 = c[n-2];           // = c[k + 0]
        c1 = c[n-1];           // = c[k + 1]
        for (int k = n - 2; k > 0; k--){
            double a = (2*k + 3) / (double) ((k + 1) * (k + 1));
            auto tmp = c0;
            c0 = c[k - 1] - c1 * sqrt(a * k * k / (double) (2*k - 1));
            c1 = tmp + c1 * x * sqrt(a * (2*k + 1));
        }
    }
    return SQRT_ONE_HALF * c0 + SQRT_THREE_HALF * c1 * x;
}


template< typename T >
inline vector<T> calc_coeffs(const vector<T>& vals_, const vector<double>& nodes, const arma::mat vmat, const vector<double>& newton_coeffs, const vector<double>& alpha, const vector<double>& gamma) {

    auto vals{vals_};
    auto b{newton_coeffs};

    int n = vals.size();
    if (n != vmat.n_rows || n != vmat.n_cols) throw std::runtime_error("calc_coeffs: vmat != n x n");
    vector<T> coeffs(n, 0);

    auto is_nan = [](const T& x) { return (isnan(std::abs(x)) || isinf(std::abs(x))); };
    auto nan_values = std::any_of(vals.begin(), vals.end(), is_nan );

    int m;
    if ( nan_values ) {
        std::replace_if(vals.begin(), vals.end(), is_nan, 0 );
        m = b.size() - 2;
    };

    // matrix vector product: coeff = vmat @ vals
    for (int i = 0; i < n; i++){
        T sum = 0;
        for (int j = 0; j < n; j++){
            sum += vmat(i, j) * vals[j];
        }
        coeffs[i] = sum;
    }

    if ( nan_values ) {
        for (int i = 0; i < n; i++){
            if (is_nan(vals_[i])){
                b[m + 1] /= alpha[m];
                auto x = nodes[i];
                b[m] = (b[m] + x * b[m + 1]) / alpha[m - 1];
                for (int j = m - 1; j > n; j--){
                    b[j] = ((b[j] + x * b[j + 1] - gamma[j + 1] * b[j + 2]) / alpha[j - 1]);
                }
                b.erase(b.begin());
                for (int j = 0; j < m; j++){
                    coeffs[j] -= coeffs[m] / b[m] * b[j];
                }
                coeffs[m] = 0;
                m -= 1;
            }
        }
    };
    return coeffs;
}


template< typename T=double > 
double norm(const vector<T>& vec) {
    double sum = 0;
    for (const auto& elem : vec){
        auto x = std::abs(elem);
        sum += x * x;
    }
    return sqrt(sum);
}


template< typename T >
inline vector<T> matvec(const arma::mat& mat, const vector<T>& vec){
    // matrix vector product mat @ vec
    if ( mat.n_cols < vec.size())
        throw std::invalid_argument("matvec: mat.n_cols < vec.size()");

    vector<T> result(mat.n_rows, 0);

    for (int i = 0; i < mat.n_rows; i++){
        T sum = 0;
        for (int j = 0; j < vec.size(); j++){
            sum += mat(i, j) * vec[j];
        }
        result[i] = sum;
    }
    return result;
}


class ParamsCquad{

  public:

    double eps = 1E-15; // TODO

    double min_sep = 16 * eps;
    int min_level = 1;
    int max_level = 4;
    double hint = 0.1;
    int ndiv_max = 20;

    vector<int> sizes;
    vector<vector<double>> nodes, newton_coeffs, center_nodes;
    vector<arma::mat> V, inv_Vs, Ts;
    vector<double> V_cond_nums;
    vector<double> alpha;
    vector<double> gamma;

    ParamsCquad(){

        // Points of the Clenshaw-Curtis rule.
        for (int level = 0; level <= max_level; level++)
            sizes.push_back( pow(2, (level + 1)) + 1 );        
        for (auto const& n : sizes)
            nodes.push_back( chebyshev_nodes(n) );

        // center nodes (left and right endpoint missing)
        for (auto const& node : nodes){
            auto cn = node;
            cn.erase(cn.begin());
            cn.pop_back();
            center_nodes.push_back( cn );
        }
              
        // Vandermonde-like matrices and their condition numbers
        for (auto const& node : nodes){
            auto vm = vandermonde(node);
            auto vi = vm.i();
            V.push_back( vm );
            inv_Vs.push_back( vi );
            V_cond_nums.push_back( arma::norm(vm, 2) * arma::norm(vi, 2) );
        }

        // Shift matrices
        auto vs = inv_Vs.back();
        vector<double> n0, n1;
        for (auto const & node : nodes.back()){
            n0.push_back( 0.5 * (node + 1) );
            n1.push_back( 0.5 * (node - 1) );
        }
        Ts.push_back( vs * vandermonde(n0) );
        Ts.push_back( vs * vandermonde(n1) );        

        // Newton polynomials
        newton_coeffs = newton_legendre(sizes);

        // Other downdate matrices
        for (int k = 0; k < sizes.back(); k++)
            alpha.push_back( sqrt((k+1)* (k+1) / (double) (2*k+1) / (double) (2*k+3)) );

        gamma.push_back(0);
        gamma.push_back(0);
        for (int k = 2; k < sizes.back(); k++){
            auto k2 = k * k;
            gamma.push_back( sqrt(k2 / (double) (4*k2-1)) );
        }
    }
};


template< typename T=double >
class Interval{

  public:

    double a, b;
    int level, depth;
    int ndiv = 0;
    double err = 1;  // initially unknown
    T igral = 0;
    T c00 = 0;
    bool unreliable_err = false;

    Interval() = default;
    Interval(function<vector<T>(vector<double>)> func_, double a_, double b_, int level_, int depth_, const ParamsCquad& params_)
    : func{func_}
    , level{level_}
    , depth{depth_}
    , params{params_}
    {
        if (a_ >= b_)
            throw std::invalid_argument("Interval: bounds wrong, a >= b");
        a = a_;
        b = b_;
        pp = 0.5 * (b + a);
        mm = 0.5 * (b - a);
        mmi = 1. / mm;
    };

    T operator() (double x) const {
        if (x < a || x > b) throw std::invalid_argument("Interval::operator(): x not inside [a, b]");
        auto y = (x - pp) * mmi;
        return eval_legendre<T>(coeffs, y);
    };

    void interpolate(){
        auto vals = func(map_coords(params.nodes[level]));
        auto coeffs = calc_coeffs<T>(vals, params.nodes[level], params.inv_Vs[level], params.newton_coeffs[level], params.alpha, params.gamma);
        auto coeffs_diff = norm<T>(coeffs); // TODO: arma::norm ?
        _interpolate(vals, coeffs, coeffs_diff);
        c00 = 0.0;
    }

    void interpolate_inside(T fl, T fr, const vector<T>& coeffs_old){
        auto cpoints = params.center_nodes[params.min_level];
        auto vals = func(map_coords(cpoints));
        vals.insert(vals.begin(), fl);
        vals.push_back(fr);
        interpolate_from_values(vals, coeffs_old);
    }

    std::pair<Interval<T>, Interval<T>> split() const {

        auto m = pp;

        // left, right, center function values
        auto fl = vals[0];
        auto fr = vals.back();
        auto fm = vals[(vals.size() - 1) / 2];

        auto child0 = Interval<T>(func, a, m, params.min_level, depth + 1, params);
        auto child1 = Interval<T>(func, m, b, params.min_level, depth + 1, params);

        
        auto coeffs0_old = matvec<T>(params.Ts[0], coeffs); //arma::vec
        //vector<T> tmp (params.Ts[0] * (arma::vec) coeffs); //arma::vec
        child0.interpolate_inside(fl, fm, coeffs0_old);

        auto coeffs1_old = matvec<T>(params.Ts[1], coeffs);
        child1.interpolate_inside(fm, fr, coeffs1_old);

        child0.check_diverge(ndiv, c00);
        child1.check_diverge(ndiv, c00);

        return std::pair(child0, child1);
    }

    bool refine(){

        level += 1;
        assert (level <= params.max_level);
        
        auto points = map_coords(params.nodes[level]);

        vector<double> x;
        for (int i = 1; i < points.size(); i+=2)
            x.push_back(points[i]);


        auto fx = func(x);
        vector<T> new_vals(points.size());

        for (int i = 0; i < fx.size(); i++){
            new_vals[2 * i] = vals[i];
            new_vals[2 * i + 1] = fx[i];
        }
        new_vals[2 * fx.size()] = vals[fx.size()];

        interpolate_from_values(new_vals, coeffs);

        bool spacing_left = (points[1] - points[0] > points[0] * params.min_sep);
        bool spacing_right = (points[points.size()-1] - points[points.size()-2] > points[points.size()-2] * params.min_sep);
        bool error_significant = (err > (std::abs(igral) * params.eps * params.V_cond_nums[level]));

        return spacing_left && spacing_right && error_significant;
    }

    void print() const {
        cout << "a= " << a << " , b= " << b << ", igral= " << igral << " , err= " << err << ", level= " << level << " , depth= " << depth << "\n";
    }

    void check_diverge(int ndiv_old, T c00_old) const {

        int c00_ratio = 0;
        if (std::abs(c00_old) != 0)
            c00_ratio = (int) (std::abs(c00 / c00_old) > 2);

        int ndiv = ndiv_old + c00_ratio;

        if (ndiv > params.ndiv_max && 2*ndiv > depth){
            cout << "a= " << a << " b= " << b << " h= " << b - a << "\n";
            throw std::runtime_error("Interval::split: Possibly divergent integral");
        }
    }

  private:

    void interpolate_from_values(const vector<T>& vals, const vector<T>& coeffs_old){
        auto coeffs = calc_coeffs<T>(vals, params.nodes[level], params.inv_Vs[level], params.newton_coeffs[level], params.alpha, params.gamma);
        int n = std::max(coeffs_old.size(), coeffs.size());
        vector<T> cdiff(n, 0);
        for (int i = 0; i < coeffs_old.size(); i++){
            cdiff[i] = coeffs_old[i];
        }
        for (int i = 0; i < coeffs.size(); i++){
            cdiff[i] -= coeffs[i];
        }
        auto coeffs_diff = norm<T>(cdiff); // TODO: arma::norm ?
        _interpolate(vals, coeffs, coeffs_diff);
    }

    void _interpolate(const vector<T>& vals_, const vector<T>& coeffs_, double coeffs_diff){
        vals = vals_;
        coeffs = coeffs_;
        if (level == params.min_level)
            c00 = coeffs[0];
        auto w = b - a;
        igral = w * coeffs[0] * SQRT_ONE_HALF;
        err = w * coeffs_diff;
        unreliable_err = coeffs_diff > params.hint * norm<T>(coeffs); // todo: arma::norm(coeffs)
        // unreliable_err = coeffs_diff > params.hint * arma::norm(coeffs);
    }

    vector<double> map_coords(const vector<double>& points) const {
        int n = points.size();
        vector<double> y(n);
        for (int i = 0; i < n; i++){
            auto x = points[i];
            assert(x >= -1 && x <= 1);
            y[i] = pp + x * mm;
        }
        return y;
    };

    function<vector<T>(vector<double>)> func;
    double pp, mm, mmi;
    vector<T> coeffs, vals;
    ParamsCquad params;
    vector<double> nodes;
    arma::mat inv_Vs;
    vector<double> V_cond_nums;
    vector<double> newton_coeffs;
    vector<double> alpha;
    vector<double> gamma;
};

//-------------------------------------------------------- The cquad class  ---------------------------------

template< typename T=double >
class Cquad{
  public:

    Cquad() = default;

    // constructor for vector functions
    Cquad(function<vector<T>(vector<double>)> f, double a_, double b_, int level=MAX_LEVEL)
    : a{a_}
    , b{b_}
    {
        auto ival = Interval<T>(f, a, b, level - 1, 1, params);
        ival.interpolate();
        ivals.push_back(ival);    // Active intervals
        improve();
    }

    // constructor for scalar functions
    Cquad(function<T(double)> f, double a_, double b_, int level=MAX_LEVEL)
    : Cquad([f](vector<double> xv){vector<T> res; for (auto const& x : xv){res.push_back( f(x) );} return res;}, a_, b_, level)
    {}

    std::pair<T, double> totals() const {
        T igral = igral_excess;
        double err = err_excess;
        for (const auto& ival : ivals){
            igral += ival.igral;
            err += ival.err;
        }
        return std::make_pair(igral, err);
    }

    void improve(){

        auto& ival = ivals.back();
        bool split;
        if (ival.level == MAX_LEVEL){
            split = true;
        } else {
            if (! ival.refine()) {
                return;
            }
            split = ival.unreliable_err;
        }

        if (split) {
            auto [child0, child1] = ival.split();
            ivals.pop_back();
            ivals.push_back(child0);
            ivals.push_back(child1);
        }

        auto increasing_error = [](const Interval<T>& u, const Interval<T>& v){ return (u.err < v.err); };
        ivals.sort(increasing_error);

    }

    std::pair<T, double> improve_until(double rtol, double atol, bool print_info=false){

        if (rtol < 0 || atol < 0){
            throw std::invalid_argument("Cquad::improve_until: Tolerances must be positive.");
        }
        if (rtol == 0 && atol == 0){
            throw std::invalid_argument("Cquad::improve_until: Either rtol or atol must be nonzero.");
        }
        while (true){
            if (print_info) {
                cout << "number of intervals= " << ivals.size() << "\n";
                for (auto const& ival : ivals)
                    ival.print();
            }
            auto [igral, err] = totals();
            auto tol = std::max(atol, abs(igral) * rtol);
            if (err == 0 || err < tol || err_excess > tol > err - err_excess || ivals.size() == 0)
                return std::make_pair(igral, err);
            improve();
        }
    }

    T operator() (double xs) const {
        if (xs < a || xs > b) throw std::invalid_argument("Cquad::operator(): xs not inside [a, b]");
        for (const auto& ival : ivals){
            if (xs >= ival.a && xs <= ival.b){
                return ival(xs);
            }
        }
        throw std::invalid_argument("Cquad::operator(): xs not present in integrals.");
        return 0.;
    };

  private:

    double a, b;
    T igral_excess = 0;
    double err_excess = 0;
    std::list<Interval<T>> ivals;
    ParamsCquad params = ParamsCquad();
};


#endif // CQUAD_H
