#!/bin/bash
rm -r build
mkdir build
cd build
cmake ../ -D BOOST_ROOT=$HOME/opt/boost_1_76_0
make
